class BWAction
  attr_reader :name, :command, :icon
  def initialize(name:, command:, icon: glyph!(:default))
    @name, @command, @icon = name, command, icon
  end
  def invoke!
    if command.respond_to?(:call)
      command.call
    else
      system(command, exception: true)
    end
  end
end
