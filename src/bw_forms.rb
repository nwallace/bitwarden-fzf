require "yaml"

module BWForms
  class Base
    attr_reader :item

    def initialize(item)
      @item = item
    end
  end

  class Create
    def submit
      $client.create_item(item)
    end
  end

  class Update
    def submit
      $client.update_item(item)
    end
  end
end

module BWItems
  class Base

    attr_accessor :id, :name

    def initialize(data={})
      data.each { |field, value| send("#{field}=", value) }
    end

    def to_json
      raise NotImplementedError
    end

    def data
      raise NotImplementedError
    end

    def entry_template
      raise NotImplementedError
    end

    def process_entry!(command, entry)
      YAML.load(entry).each do |key, value|
        send("#{key}=", value)
      end
      $client.public_send("#{command}_item", self)
    end
  end

  class URI < Base

    attr_accessor :match, :uri

    def data
      {
        uri: uri,
        match: match,
      }
    end
  end

  class Login < Base

    attr_accessor :username, :password, :totp, :uris

    def data
      {
        name: name,
        username: username,
        password: password,
        # totp: totp,
        # uris: uris,
      }
    end

    def to_json
      {
        # "organizationId": null,
        # "collectionIds": null,
        # "folderId": null,
        "type": 1,
        "name": name,
        # "notes": "Some notes about this item.",
        # "favorite": false,
        # "fields": [],
        "login": {
          # "uris": uris.to_json,
          "username": username,
          "password": password,
          # "totp": "JBSWY3DPEHPK3PXP"
        },
        # "secureNote": null,
        # "card": null,
        # "identity": null,
        # "reprompt": 0
      }.to_json
    end

    def entry_template
      %Q{# This is the entry template for a Login...
name: 
username: 
password: 
# uris:
#   - uri: 
#     match: }
    end
  end

  #class Card < Base
  #  #cardholderName, brand, number, expMonth, expYear, code
  #  attr_accessor :cardholder, :brand, :number, :exp_month, :exp_year, :cvv
  #end

  #class Note < Base
  #  #?
  #  attr_accessor :text
  #end

  #class Identity < Base
  #  #title
  #  #firstName
  #  #middleName
  #  #lastName
  #  #address1
  #  #address2
  #  #address3
  #  #city
  #  #state
  #  #postalCode
  #  #country
  #  #company
  #  #email
  #  #phone
  #  #ssn
  #  #username
  #  #passportNumber
  #  #licenseNumber
  #end
end
