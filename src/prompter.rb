require "tempfile"
require_relative "./executor"

module Prompter
  class FZF
    include Executor

    attr_reader :executable

    def initialize(executable: "fzf")
      @executable = executable
    end

    def select!(options, other_actions=[])
      header_str = "Keys -- Enter: Select"
      other_actions.each do |action|
        header_str << ", #{action.key}: #{action.description}"
      end
      command_str = "#{executable} --layout=reverse --with-nth=2.. --header='#{header_str}'"
      if other_actions.any?
        command_str << " --expect=#{other_actions.map(&:key).join(",")}"
      end
      pipe = IO.popen(command_str, "w+")
      options.each_with_index do |option, i|
        pipe.puts("#{i} #{block_given? ? yield(option, i) : option}")
      end
      selection_str = pipe.read
      exit 1 if selection_str !~ /\S/
      if other_actions.any?
        lines = selection_str.lines
        captured_key = lines.shift.chomp.downcase
        other_action = other_actions.find { |a| a.key == captured_key }
        if other_action
          other_action.call
          return
        end
        selection_str = lines.join
      end
      options[selection_str[/^\d+/].to_i]
    ensure
      pipe.close
    end

    def new_item(item)
      entry_file = Tempfile.new(["fbw", ".yml"])
      entry_file.puts(item.entry_template)
      entry_file.write
      entry_file.close
      system(ENV.fetch("EDITOR", "vim"), entry_file.path)
      File.read(entry_file.path)
    ensure
      entry_file.unlink
    end
  end
end
