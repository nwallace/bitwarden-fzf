require_relative "./bw_action"

module BWItem
  class ActionBuilder

    attr_reader :label, :requirements, :icon, :block

    def initialize(label, requirements, icon, block)
      @label, @requirements, @icon, @block = label, requirements, icon, block
    end

    def meets_requirements?(data)
      requirements.all? do |required_path|
        paths = required_path.split(/[\.\[\]]+/).map { |s| s =~ /^\d+$/ ? s.to_i : s }
        value_at_path(data, paths) =~ /\S/
      end
    end

    def build!(data)
      values = if requirements.any?
                 requirements.map do |required_path|
                   paths = required_path.split(/[\.\[\]]+/).map { |s| s =~ /^\d+$/ ? s.to_i : s }
                   value_at_path(data, paths)
                 end
               else
                 [data]
               end
      BWAction.new(name: label, icon: icon, command: -> { block.call(*values) })
    end

    def value_at_path(data, paths)
      if paths.any? && !data.nil?
        next_path = paths.shift
        value_at_path(data[next_path], paths)
      else
        data
      end
    end
  end

  def self.build(data)
    case data["type"]
    when 1
      Login
    when 2
      Note
    when 3
      Card
    when 4
      Identity
    else
      raise ArgumentError, "Unknown item type #{data["type"]}"
    end.new(data)
  end

  class Base

    def self.icon(glyph)
      define_method(:icon) do
        glyph
      end
    end

    def self.action(label, with:, icon:, &block)
      action_builders << ActionBuilder.new(label, with, icon, block)
    end

    def self.inherited(base)
      super
      def base.action_builders
        @action_builders ||= []
      end
    end

    attr_reader :data

    def initialize(data)
      @data = data
    end

    def name
      data["name"]
    end

    def actions
      @actions ||= (self.class.action_builders + self.class.global_builders).each_with_object([]) do |builder, actions|
        actions << builder.build!(data) if builder.meets_requirements?(data)
      end + field_actions
    end

    def copy_to_clipboard(*args)
      self.class.copy_to_clipboard(*args)
    end

    def self.copy_to_clipboard(string, type=:default)
      opts = ["--trim-newline"]
      opts << "--primary" if type == :primary
      cmd = "wl-copy #{opts.join(" ")} #{string.shellescape}"
      system("setsid", "wl-copy", *opts, string, exception: true)
    end

    def type(*args)
      self.class.type(*args)
    end

    def type_special_key(*args)
      self.class.type_special_key(*args)
    end

    def self.type(*args)
      system("wtype", *args, exception: true)
    end

    def self.type_special_key(*args)
      system("wtype", "-k", *args, exception: true)
    end

    # TODO: not working
    def self.xdg_open(uri)
      pid = fork { exec("setsid", "xdg-open", uri) }
      Process.detach(pid)
    end

    private

    def self.global_builders
      @global_builders ||= [
        ActionBuilder.new("Display notes", %w[notes], glyph!(:note), ->(notes) {
          IO.popen([{"LESS" => ""}, ENV.fetch("PAGER", "less")], "w") { |p| p.puts notes }
        }),
        ActionBuilder.new("Display everything", [], glyph!(:view_all), ->(data) {
          IO.popen([{"LESS" => ""}, ENV.fetch("PAGER", "less")], "w") do |p|
            p.puts data.to_yaml.lines[1..-1]
          end
        }),
      ]
    end

    def field_actions
      data.fetch("fields", {}).map do |field|
        BWAction.new(icon: glyph!(:copy), name: "Copy #{field["name"]}", command: -> { copy_to_clipboard(field["value"]) })
      end
    end
  end

  class Login < Base
    icon glyph!(:account)

    action("Copy username & password, open URI", with: %w[login.username login.password login.uris[0].uri], icon: glyph!(:do_all)) do |username, password, uri|
      copy_to_clipboard(username)
      copy_to_clipboard(password, :primary)
      xdg_open(uri)
    end

    action("Copy username & password", with: %w[login.username login.password], icon: glyph!(:copy)) do |username, password|
      copy_to_clipboard(username)
      copy_to_clipboard(password, :primary)
    end

    action("Type username & password", with: %w[login.username login.password], icon: glyph!(:keyboard)) do |username, password|
      system("swaymsg", "move", "container", "to", "scratchpad") # HACK
      type(username)
      type_special_key("Tab")
      type(password)
    end

    action("Copy password", with: %w[login.password], icon: glyph!(:copy)) { |password| copy_to_clipboard(password) }

    action("Copy username", with: %w[login.username], icon: glyph!(:copy)) { |username| copy_to_clipboard(username) }

    action("Copy one-time password", with: %w[id login.totp], icon: glyph!(:copy)) do |id, totp|
      totp = $client.get_totp(id)
      copy_to_clipboard(totp)
    end

    action("Open URI", with: %w[login.uris[0].uri], icon: glyph!(:out)) { |uri| xdg_open(uri) }
  end

  class Note < Base
    icon glyph!(:note)
  end

  class Card < Base
    icon glyph!(:card)

    action("Copy number & CVV", with: %w[card.number card.code], icon: glyph!(:copy)) do |number, code|
      copy_to_clipboard(number)
      copy_to_clipboard(code, :primary)
    end

    action("Copy number", with: %w[card.number], icon: glyph!(:copy)) { |number| copy_to_clipboard(number) }

    action("Copy CVV", with: %w[card.code], icon: glyph!(:copy)) { |cvv| copy_to_clipboard(code) }

    action("Copy expiration", with: %w[card.expMonth card.expYear], icon: glyph!(:copy)) { |mo, yr| copy_to_clipboard([mo, yr].join("/")) }

    action("Copy cardholder", with: %w[card.cardholderName], icon: glyph!(:copy)) { |name| copy_to_clipboard(name) }
  end

  class Identity < Base
    icon glyph!(:identity)
  end
end
