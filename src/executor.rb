module Executor
  def exec!(*args)
    result = exec(*args)
    raise "Unexpected exit status #{result.exit_status} from command #{result.command}" unless result.success?
    result.stdout
  end

  def exec(*args)
    command = "#{executable} #{args.join(" ")}"
    stdout = `#{command}`
    ExecutionResult.new(command, $?, stdout)
  end

  class ExecutionResult
    attr_reader :command, :exit_status, :stdout
    def initialize(command, exit_status, stdout)
      @command = command
      @exit_status = exit_status
      @stdout = stdout
    end
    def success?
      exit_status == 0
    end
  end
end
