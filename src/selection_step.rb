require_relative "bw_forms"

module SelectionStep

  class KeyAction

    attr_reader :key, :description

    def initialize(key:, description:, &block)
      @key, @description, @block = key, description, block
    end

    def call
      @block.call
    end
  end

  class Base

    attr_reader :prompter, :items, :prev

    def initialize(prompter, items, prev: nil)
      @prompter, @items, @prev = prompter, items, prev
    end

    def forward!(selected_item)
      raise NotImplementedError
    end

    def back!
      if prev
        prev.prompt!
      else
        exit 1
      end
    end

    def prompt!
      other_actions = []
      if prev
        other_actions << KeyAction.new(key: "ctrl-h", description: "Go back") { back! }
      end
      other_actions << KeyAction.new(key: "ctrl-n", description: "New login") do
        login = BWItems::Login.new
        login.process_entry!(:create, prompter.new_item(login))
      end
      other_actions += key_actions
      selected_item = prompter.select!(items, other_actions) do |item|
        format_for_selection(item)
      end
      forward!(selected_item) if selected_item
    end

    def key_actions
      []
    end

    private

    def format_for_selection(item)
      raise NotImplementedError
    end
  end

  class Item < Base

    def forward!(selected_item)
      Action.new(prompter, selected_item.actions, prev: self).prompt!
    end

    def key_actions
      [
        KeyAction.new(key: "ctrl-r", description: "Reload vault") do
          $starter.call(reload: true)
        end
      ]
    end

    private

    def format_for_selection(item)
      "#{item.icon} #{item.name}"
    end
  end

  class Action < Base

    def forward!(selected_action)
      selected_action.invoke!
    end

    private

    def format_for_selection(action)
      "#{action.icon} #{action.name}"
    end
  end
end
