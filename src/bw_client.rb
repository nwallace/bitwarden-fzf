require "json"
require "shellwords"
require_relative "./executor"
require_relative "./bw_item"

class BWClient
  include Executor

  attr_reader :executable, :token

  def initialize(executable: "bw")
    @executable = executable
    token = `keyctl pipe "$(keyctl request user bw_session)" 2> /dev/null`
    if token =~ /\S/
      @token = token
    end
  end

  def logged_in?
    status != "unauthenticated"
  end

  def log_in!
    token = exec!("login --raw").chomp
    store_token!(token)
  end

  def locked?
    is_locked = !exec("unlock --check").success?
    clear_token! if is_locked
    is_locked
  end

  def unlock!
    token = exec!("unlock --raw").chomp
    if token !~ /\S/
      print "Unlocking failed. Try again? (Y/n) "
      answer = gets.chomp
      if answer.casecmp("y")
        unlock!
      else
        exit 1
      end
    end
    store_token!(token)
  end

  def get_items
    @items ||=
      exec_json!("list items")
        .map { |item| BWItem.build(item) }
  end

  def clear_cache!
    @items = nil
  end

  def sync!
    clear_cache!
    exec!("sync")
  end

  def get_totp(item_id)
    exec!("get totp #{item_id.shellescape}").chomp
  end

  def create_item(item)
    encoded = exec!("encode", "<<<", item.to_json.shellescape)
    exec!("create", "item", "<<<", encoded.shellescape)
    prompt_to_exit
  end

  def update_item(item)
    encoded = exec!("encode", item.to_json.shellescape)
    exec!("edit", "item", item.id, encoded.shellescape)
    prompt_to_exit
  end

  private

  def prompt_to_exit
    puts "Press enter to exit"
    gets.chomp
  end

  def exec(*args)
    args.unshift("--session=#{token.shellescape}") if token
    super(*args)
  end

  def exec_json!(*args)
    JSON.parse(exec!(*args))
  end

  def status
    @status ||= exec_json!("status")["status"]
  end

  def store_token!(token)
    @token = token
    `keyctl add user bw_session '#{token}' @u`
  end

  def clear_token!
    @token = nil
    `keyctl unlink $(keyctl request user bw_session) @u`
  end
end
